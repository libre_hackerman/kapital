# Kapital

Kapital is a simple balance manager that keeps a record of your spendings and earnings on text files on a weekly basis.

### CLI arguments
- `-a, --add-record`: prompts the user for the ammount of spending/earning and its reason and saves it.
- `-b, --current-balance`: calculates the current balance
- `-w --weekly-log`: displays a table with all the spendings and earnings of the week.

### Logs
Kapital will store all the logs in `~/.local/share/kapital`, with each file containing in plain text the records of each week. This is an example:

*~/.local/share/kapital/28-02-2022.log*
```
balance_starting_week:80.00
02-03-2022:-24.00:Groceries
03-03-2022:-1.20:Coffee
03-03-2022:20.00:A gift

```

### Build

#### Dependencies
- Make
- C compiler

#### Compilation
Just run `make`. If you want debug symbols, run `make debug`

#### Installation
You can directly run the compiled executable, but if you want to install it on your system, `make install` will do it. Notice that you can set the variable `PREFIX` (default to /usr/local) to your desire

#### Uninstall
`make uninstall` (Mind the `PREFIX` too)
