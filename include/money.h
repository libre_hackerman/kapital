/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MONEY_H
#define MONEY_H

#include <stdbool.h>

#define CURRENCY "eur"
#define REGEX_MONEY "[-\\+]?[0-9]+(\\.[0-9]{2})?"

typedef int money_t;

/*
 * Converts the given string to money_t. Returns true on success.
 */
bool
parse_money(money_t* money, const char* str_money);

/*
 * Converts a money_t to a heap allocated string. Returns NULL on error.
 */
char*
money_to_string(money_t money);

#endif
