/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ARGS_H
#define ARGS_H

#include <stdbool.h>

struct args
{
	bool add_record;
	bool current_balance;
	bool show_weekly_log;
};

/*
 * This function parses the given CLI arguments and stores them in the passed
 * struct args. Returns false if the CLI arguments are wrong.
 */
bool
parse_arguments(struct args* args, int argc, char *argv[]);

#endif
