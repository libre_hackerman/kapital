/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <regex.h>

#include <money.h>

/*
 * This function receives a string containing what should be a money ammount
 * as described in the regex. Example: 123, 34.04, -456, -8.43, +456, +8.43
 */
static bool
is_money(const char* str_money)
{
	regex_t money_regex;
	bool indeed_is;

	if (regcomp(&money_regex, "^" REGEX_MONEY "$", REG_EXTENDED))
	{
		fputs("Error compiling money regex\n", stderr);
		return (false);
	}
	indeed_is = regexec(&money_regex, str_money, 0, NULL, 0) == 0;
	regfree(&money_regex);
	return (indeed_is);
}

bool
parse_money(money_t* money, const char* str_money)
{
	int sign;
	int integer_part;
	int decimal_part;

	if (!str_money)
		return (false);
	if (strlen(str_money) == 0 || !is_money(str_money))
	{
		fprintf(stderr, "Invalid money ammount: %s\n", str_money);
		return (false);
	}

	if ((sign = str_money[0] == '-' ? -1 : 1) == -1)
		str_money++;
	integer_part = atoi(str_money);
	decimal_part = strchr(str_money, '.') ? atoi(strchr(str_money, '.') + 1) : 0;
	*money = integer_part * 100 + decimal_part;
	*money *= sign;
	return (true);
}

char*
money_to_string(money_t money)
{
	size_t digits;
	char* str_money;

	digits = snprintf(NULL, 0, "%d", money);
	if (!(str_money = calloc(digits + 4, sizeof(char))))
	{
		perror("Memory allocation error");
		return (NULL);
	}
	sprintf(str_money, "%s%d.%02d", money < 0 ? "-" : "",
			abs(money) / 100, abs(money) % 100);
	return (str_money);
}
