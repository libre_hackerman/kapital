/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include <args.h>

static void
display_help(const char *prog_name)
{
	const int opt_width = 23;

	printf("Usage: %s [-a | -m | -w] [-h]\n\n", prog_name);
	puts("Utility to keep track of money spendings\n");
	puts("CLI arguments:");
	printf("%-*sAdd a record (spending/ingress)\n",
			opt_width, "-a, --add-record");
	printf("%-*sGet current balance\n",
			opt_width, "-b, --current-balance");
	printf("%-*sShow weekly log\n",
			opt_width, "-w, --weekly-log");
	exit(EXIT_SUCCESS);
}

bool
parse_arguments(struct args* args, int argc, char *argv[])
{
	int op;

	struct option long_options[] = {
		{"add-record", no_argument, NULL, 'a'},
		{"current-balance", no_argument, NULL, 'b'},
		{"weekly-log", no_argument, NULL, 'w'},
		{"help", no_argument, NULL, 'h'},
	};

	args->add_record = args->current_balance = args->show_weekly_log = false;

	while ((op = getopt_long(argc, argv, "abwh", long_options, NULL)) != -1)
	{
		switch (op)
		{
			case 'a':
				if (args->current_balance || args->show_weekly_log)
				{
					fputs("Wrong CLI arguments\n", stderr);
					return (false);
				}
				args->add_record = true;
				break;
			case 'b':
				if (args->add_record || args->show_weekly_log)
				{
					fputs("Wrong CLI arguments\n", stderr);
					return (false);
				}
				args->current_balance = true;
				break;
			case 'w':
				if (args->add_record || args->current_balance)
				{
					fputs("Wrong CLI arguments\n", stderr);
					return (false);
				}
				args->show_weekly_log = true;
				break;
			case 'h':
				display_help(argv[0]);
				break;
			default:
				return (false);
		}
	}

	if (!args->add_record && !args->current_balance && !args->show_weekly_log)
	{
		fputs("You need to set either -a, -b, or -w\n", stderr);
		return (false);
	}
	return (true);
}
