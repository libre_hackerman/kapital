#include <stdio.h>
#include <stdlib.h>

#include <args.h>
#include <log.h>

int
main(int argc, char *argv[])
{
	struct args args;

	if (!parse_arguments(&args, argc, argv))
		return (EXIT_FAILURE);

	if (args.add_record)
		add_record();
	else if (args.current_balance)
		current_balance();
	else if (args.show_weekly_log)
		show_weekly_log();

	return (EXIT_SUCCESS);
}
