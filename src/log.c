/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <regex.h>
#include <errno.h>

#include <log.h>
#include <money.h>

#define FOR_RED "\033[31m"
#define FOR_GREEN "\033[32m"
#define RESET "\033[0m"

struct record
{
	char date[11];
	money_t diff;
	char* reason;
};

struct log
{
	money_t balance_starting_week;
	struct record* records;
	size_t n_records;
};

/*
 * Create all the directories in the specified path if they don't exist
 */
static void
mkdir_parents(const char *path)
{
	char* dir;
	size_t dir_len;
	char* path_slash_ended;
	const char* cur_separator;

	if (!(path_slash_ended = malloc(strlen(path) + 2)))
	{
		perror("Memory allocation error");
		return;
	}
	sprintf(path_slash_ended, "%s/", path);
	cur_separator = path_slash_ended;

	if (!(dir = malloc((dir_len = strlen(path_slash_ended) + 1))))
	{
		perror("Memory allocation error");
		goto exit_1;
	}
	while ((cur_separator = strchr(cur_separator + 1, '/')))
	{
		memset(dir, '\0', dir_len);
		memcpy(dir, path_slash_ended, cur_separator - path_slash_ended);

		/* Create dir if it doesn't exists */
		if (access(dir, F_OK) == -1)
		{
			if (mkdir(dir, 0777) == -1)
			{
				perror(dir);
				goto exit_2;
			}
		}
	}

	exit_2: free(dir);
	exit_1: free(path_slash_ended);
}

/*
 * Stores the current week in the given buffer, which MUST be at least of
 * size 11.
 */
static void
get_current_week(char* current_week)
{
	time_t now;
	struct tm tm;

	now = time(NULL);
	localtime_r(&now, &tm);
	/* Offset the time for monday */
	if (tm.tm_wday != 1)
	{
		if (tm.tm_wday == 0) // If sunday
			now -= 86400 * 6;
		else
			now -= 86400 * (tm.tm_wday - 1);
		localtime_r(&now, &tm);
	}
	sprintf(current_week, "%02d-%02d-%d", tm.tm_mday, tm.tm_mon + 1,
			tm.tm_year + 1900);
}

/*
 * Returns a heap allocated string with the path for the logs directory or
 * NULL in case of error.
 */
static char*
get_logs_dir()
{
	char* home;
	char* logs_dir;

	if (!(home = getenv("HOME")))
	{
		fputs("HOME environment variable is not set\n", stderr);
		return (NULL);
	}
	if (!(logs_dir = malloc(strlen(home) + strlen(LOGS_DIR_IN_HOME) + 2)))
	{
		perror("Memory allocation error");
		return (NULL);
	}
	sprintf(logs_dir, "%s/%s", home, LOGS_DIR_IN_HOME);
	return (logs_dir);
}

/*
 * Returns a heap allocated string with the absolute path to the current
 * log file or NULL in case of error.
 */
static char*
get_current_log()
{
	char current_week[11];
	char* logs_dir;
	char* current_log;

	current_log = NULL;
	get_current_week(current_week);
	if (!(logs_dir = get_logs_dir()))
		goto exit_0;
	if (!(current_log = malloc(strlen(logs_dir) + 17)))
	{
		perror("Memory allocation error");
		goto exit_1;
	}
	sprintf(current_log, "%s/%s.log", logs_dir, current_week);

	exit_1: free(logs_dir);
	exit_0: return (current_log);
}

/*
 * Return the time_t associated to the given logfile name.
 */
static time_t
get_time_for_logfile(const char* logfile_name)
{
	struct tm tm;

	memset(&tm, 0, sizeof(struct tm));
	strptime(logfile_name, "%d-%m-%Y.log",&tm);
	return (mktime(&tm));
}

/*
 * Returns a heap allocated string with the absolute path to the most recent
 * log file in the logs folder. If there are no logs, returns an empty string.
 * Returns NULL in case of error.
 */
static char*
get_most_recent_log()
{
	char* logs_dir_path;
	DIR* logs_dir;
	struct dirent *dir_ent;
	regex_t logfile_regex;
	time_t logfile_time, biggest_time;
	char* most_recent_log;
	char* full_most_recent_log;

	full_most_recent_log = NULL;
	if (!(logs_dir_path = get_logs_dir()))
		goto exit_0;
	if (!(logs_dir = opendir(logs_dir_path)))
	{
		perror(logs_dir_path);
		goto exit_1;
	}
	if (regcomp(&logfile_regex, "^[0-9]{2}-[0-9]{2}-[0-9]{4}\\.log$", REG_EXTENDED))
	{
		fputs("Error compiling logfile regex\n", stderr);
		goto exit_2;
	}

	biggest_time = 0;
	most_recent_log = NULL;
	while ((dir_ent = readdir(logs_dir)))
	{
		if (dir_ent->d_type == DT_REG &&
				regexec(&logfile_regex, dir_ent->d_name, 0, NULL, 0) == 0)
		{
			logfile_time = get_time_for_logfile(dir_ent->d_name);
			if (logfile_time > biggest_time)
			{
				free(most_recent_log);
				most_recent_log = strdup(dir_ent->d_name);
				biggest_time = logfile_time;
			}
		}
	}

	if (most_recent_log) // If at least one logfile was found
	{
		if (!(full_most_recent_log = malloc(strlen(logs_dir_path) +
						strlen(most_recent_log) + 2)))
		{
			perror("Memory allocation error");
			goto exit_3;
		}
		sprintf(full_most_recent_log, "%s/%s", logs_dir_path, most_recent_log);
	}
	else
		full_most_recent_log = strdup("");

	exit_3:
		free(most_recent_log);
		regfree(&logfile_regex);
	exit_2: closedir(logs_dir);
	exit_1: free(logs_dir_path);
	exit_0: return (full_most_recent_log);
}

/*
 * Reads a line from the passed FILE (removing the newline) and returns it
 * in a heap allocated string. Returns NULL on error or EOL.
 */
static char*
get_file_line(FILE* f)
{
	char* line;
	size_t getline_n;
	size_t line_len;

	line = NULL;
	getline_n = 0;
	errno = 0;
	if (getline(&line, &getline_n, f) == -1)
	{
		if (errno)
			perror("getline");
		free(line);
		return (NULL);
	}

	line_len = strlen(line);
	if (line_len > 1 && line[line_len - 1] == '\n')
		line[line_len - 1] = '\0';
	return (line);
}

/*
 * This function checks that the passed log file is valid, returning the
 * ammount of records it contains. If it's invalid or there's an error or there
 * aren't any logs, it returns -1.
 */
static int
validate_log(const char* logfile_name)
{
	FILE* f;
	int n_records;
	char* line;
	regex_t header_regex, record_regex;

	n_records = -1;
	if (!(f = fopen(logfile_name, "r")))
	{
		perror(logfile_name);
		return (n_records);
	}
	if (regcomp(&header_regex, "^balance_starting_week:" REGEX_MONEY "$", REG_EXTENDED))
	{
		fputs("Error compiling logfile header regex\n", stderr);
		goto exit_1;
	}
	if (regcomp(&record_regex, "^([0-9]{2}-){2}[0-9]{4}:" REGEX_MONEY ":.*$", REG_EXTENDED))
	{
		fputs("Error compiling logfile header regex\n", stderr);
		goto exit_2;
	}

	// Check logfile header
	if (!(line = get_file_line(f)) ||
			regexec(&header_regex, line, 0, NULL, 0) != 0)
	{
		fprintf(stderr, "Invalid header in %s: %s\n", logfile_name, line);
		free(line);
		goto exit_3;
	}
	free(line);

	// Check records
	n_records = 0;
	while ((line = get_file_line(f)))
	{
		if (regexec(&record_regex, line, 0, NULL, 0) == 0)
			n_records++;
		else
		{
			fprintf(stderr, "Invalid record in %s: %s\n", logfile_name, line);
			free(line);
			n_records = -1;
			break;
		}
		free(line);
	}

	exit_3: regfree(&record_regex);
	exit_2:	regfree(&header_regex);
	exit_1:	fclose(f);
	return (n_records);
}

/*
 * This function deletes the memory allocated for a struct log.
 */
static void
delete_log(struct log* log)
{
	if (log)
	{
		for (size_t i = 0; i < log->n_records; i++)
			free(log->records[i].reason);
		free(log->records);
		free(log);
	}
}

/*
 * This function parses the most recent log. Returns NULL in case of error.
 */
static struct log*
parse_log()
{
	char* most_recent_log;
	int n_records;
	struct log* log;
	FILE* f;
	char* line;

	log = NULL;
	if (!(most_recent_log = get_most_recent_log()))
		goto exit_0;

	if (strlen(most_recent_log) > 0 &&
			((n_records = validate_log(most_recent_log)) == -1))
		goto exit_1;

	if (!(log = malloc(sizeof(struct log))))
	{
		perror("Memory allocation error");
		goto exit_1;
	}

	// If there are no logfiles
	if (strlen(most_recent_log) == 0)
	{
		log->balance_starting_week = 0;
		log->n_records = 0;
		log->records = NULL;
		goto exit_1;
	}

	if (!(f = fopen(most_recent_log, "r")))
	{
		perror(most_recent_log);
		goto exit_3;
	}

	if (!(line = get_file_line(f)))
		goto exit_3;
	if (!parse_money(&(log->balance_starting_week), strchr(line, ':') + 1))
	{
		fprintf(stderr, "Invalid header in %s: %s\n", most_recent_log, line);
		free(line);
		goto exit_3;
	}
	free(line);

	log->n_records = n_records;
	if (!(log->records = calloc(n_records, sizeof(struct record))))
	{
		perror("Memory allocation error");
		goto exit_3;
	}

	for (int i = 0; i < n_records; i++)
	{
		if (!(line = get_file_line(f)))
			goto exit_3;
		strcpy(log->records[i].date, strtok(line, ":"));

		if (!parse_money(&(log->records[i].diff), strtok(NULL, ":")))
		{
			free(line);
			goto exit_3;
		}

		log->records[i].reason = strdup(strtok(NULL, ":"));
		free(line);
	}

	goto exit_2;  // Only free log if it's an error
	exit_3:
		delete_log(log);
		log = NULL;

	exit_2: fclose(f);
	exit_1: free(most_recent_log);
	exit_0: return (log);
}

/*
 * This function returns a heap allocated string with the current balance.
 * Returns NULL in case of error.
 */
static char*
get_current_balance()
{
	struct log* log;
	money_t balance;
	char* balance_str;

	if (!(log = parse_log()))
		return (NULL);

	balance = log->balance_starting_week;
	for (size_t i = 0; i < log->n_records; i++)
		balance += log->records[i].diff;

	balance_str = money_to_string(balance);
	delete_log(log);
	return (balance_str);
}

/*
 * Creates the logs directory if it doesn't exists.
 */
static void
create_logs_dir()
{
	char* logs_dir;

	if (!(logs_dir = get_logs_dir()))
		return;

	mkdir_parents(logs_dir);
	free(logs_dir);
}

/*
 * Adds the given record to the current weekly log, creating the file
 * if it doesn't exists.
 */
static void write_new_record(struct record* rec)
{
	char* current_log;
	bool log_exists;
	FILE* f;
	char* current_balance;
	char* diff;

	create_logs_dir();
	if (!(current_log = get_current_log()))
		return;

	current_balance = NULL;
	if (!(log_exists = access(current_log, F_OK) == 0))
		if (!(current_balance = get_current_balance()))
			goto exit_0;

	if (!(f = fopen(current_log, "a")))
	{
		perror(current_log);
		goto exit_1;
	}

	if (!log_exists)
		fprintf(f, "balance_starting_week:%s\n", current_balance);

	if ((diff = money_to_string(rec->diff)))
	{
		fprintf(f, "%s:%s:%s\n", rec->date, diff, rec->reason);
		free(diff);
	}

	fclose(f);
	exit_1: free(current_balance);
	exit_0: free(current_log);
}

/*
 * Displays the current balance.
 */
void
current_balance()
{
	char* current_balance;

	if ((current_balance = get_current_balance()))
	{
		printf("Current balance: %s%s%s %s\n",
				*current_balance == '-' ? FOR_RED : FOR_GREEN,
				current_balance, RESET, CURRENCY);
		free(current_balance);
	}
}

/*
 * Prompts the user to add a new record to the current weekly log.
 */
void
add_record()
{
	struct record rec;
	time_t now;
	char* diff;

	now = time(NULL);
	strftime(rec.date, sizeof(rec.date), "%d-%m-%Y", localtime(&now));

	while (true)
	{
		fputs("Ammount of spending/ingress: ", stdout);
		diff = get_file_line(stdin);

		if (diff && parse_money(&rec.diff, diff))
		{
			free(diff);
			break;
		}
		else
		{
			puts("Invalid ammount. Valid ammounts are:");
			puts("123, 34.04, -456, -8.43, +456, +8.43 ...\n");
			free(diff);
		}
	}

	do
	{
		fputs("Reason: ", stdout);
		rec.reason = get_file_line(stdin);
		if (strchr(rec.reason, ':'))
		{
			free(rec.reason);
			rec.reason = NULL;
			puts("Reason can't contain character ':'");
		}
	}
	while (!rec.reason);

	write_new_record(&rec);
	free(rec.reason);
	current_balance();
}

/*
 * This function computes the required sizes for the diff and reason size
 * given the passed log for the weekly table. Returns false on error.
 */
static bool
get_table_sizes(struct log* log, size_t* diff_size, size_t* reason_size)
{
	char* diff_str;
	size_t len_diff_str;
	size_t len_reason;

	*diff_size = strlen("Diff");
	*reason_size = strlen("Reason");
	for (size_t i = 0; i < log->n_records; i++)
	{
		if (!(diff_str = money_to_string(log->records[i].diff)))
			return (false);
		len_diff_str = strlen(diff_str);
		if (len_diff_str > *diff_size)
			*diff_size = len_diff_str;
		free(diff_str);

		len_reason = strlen(log->records[i].reason);
		if (len_reason > *reason_size)
			*reason_size = len_reason;
	}
	return (true);
}

/*
 * Prints a horizontal separator for the weekly table.
 */
static void
print_horizontal_separator(size_t diff_size, size_t reason_size)
{
	fputs("+------------+-", stdout);
	for (size_t i = 0; i < diff_size; i++)
		putchar('-');
	fputs("-+-", stdout);
	for (size_t i = 0; i < reason_size; i++)
		putchar('-');
	puts("-+");
}

/*
 * Displays a table with the current week activity
 */
void
show_weekly_log()
{
	struct log* log;
	size_t diff_size, reason_size;
	char* diff_str;
	char* balance_starting_week;

	if (!(log = parse_log()))
		return;

	if (!get_table_sizes(log, &diff_size, &reason_size))
		goto exit_1;

	if (!(balance_starting_week = money_to_string(log->balance_starting_week)))
		goto exit_1;
	printf("Balance starting week: %s%s%s\n",
			*balance_starting_week == '-' ? FOR_RED : FOR_GREEN,
			balance_starting_week, RESET);
	free(balance_starting_week);

	if (log->n_records > 0)
	{
		// Header of the table
		print_horizontal_separator(diff_size, reason_size);
		printf("| Date       | %-*s | %-*s |\n", (int)diff_size, "Diff",
				(int)reason_size, "Reason");
		print_horizontal_separator(diff_size, reason_size);

		// Body of the table
		for (size_t i = 0; i < log->n_records; i++)
		{
			if (!(diff_str = money_to_string(log->records[i].diff)))
				goto exit_1;
			printf("| %s | %s%-*s%s | %-*s |\n", log->records[i].date,
					*diff_str == '-' ? FOR_RED : FOR_GREEN,
					(int)diff_size, diff_str, RESET,
					(int)reason_size, log->records[i].reason);
			print_horizontal_separator(diff_size, reason_size);
			free(diff_str);
		}
	}
	current_balance();

	exit_1: delete_log(log);
}
